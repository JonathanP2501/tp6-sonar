package agl.tp6;

import static org.junit.jupiter.api.Assertions.*;
import java.util.HashMap;
import java.util.stream.Stream;

import agl.tp6.exception.AffectationVoeuImpossibleException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.Parameterized;

class TestGroupe {
	
	public Groupe g;
	
	@BeforeEach
	public void init() {
		g = new Groupe("groupe");
	}
	
	@Parameterized.Parameters
    public static Stream<Arguments> ordreVoeu() {
        return Stream.of(
            Arguments.of(0, "Ordre de voeu trop bas (<1)"),
            Arguments.of(7, "Ordre de voeu trop haut (>6)")
        );
    }

	
	@Test
	void testGroupeIdDeuxGroupes() {
		int id = g.getId() + 1;
		Groupe gg = new Groupe("groupe");
		assertEquals(id, gg.getId());
	}

	@Test
	void testGroupeStringl() {
		assertEquals("groupe", g.getNom());
	}
	
	@Test
	void testGroupeVoeuFiniNon() {
		assertFalse(g.isVoeuReady());
	}
	
	@Test
	void testGroupeVoeuFiniOui() {
		g.setVoeuFini(true);
		assertTrue(g.isVoeuReady());
	}
	
	@Test
	void testGroupeChangementNom() {
		g.setNom("coucou");
		assertEquals("coucou", g.getNom());
	}
	
	@Test
	void testGroupeVide() {
		Groupe gg = new Groupe();
		assertEquals(null, gg.getNom());
	}
	
	@Test
	void testGroupeToString() {
		int id = g.getId();
		String nom = g.getNom();
		assertEquals(nom+" (id:"+id+")\n", g.toString());
	}
	
	@Test
	void testGroupeToStringUnVoeu() {
		Sujet s = new Sujet("suj");
		try {
			g.addVoeu(1, s);
		} catch (AffectationVoeuImpossibleException e) {
			e.printStackTrace();
		}
		int id = g.getId();
		String nom = g.getNom();
		assertEquals(nom+" (id:"+id+")\n--1: "+s.toString()+"\n", g.toString());
	}
	
	@ParameterizedTest(name = "Essayé avec l'ordre : {0}, problème: {1}")
    @MethodSource("ordreVoeu")
	void testGroupeAjouterVoeuOrdreHorsDesClous(int ordre, String probleme) {
		AffectationVoeuImpossibleException thrown = assertThrows(AffectationVoeuImpossibleException.class, () -> {g.addVoeu(ordre, null);});
        assertEquals("L'ordre du voeu doit être entre 1 et 6", thrown.getMessage());
	}
	
	@Test
	void testGroupeAjouterVoeuCorrect() {
		try {
			assertTrue(g.addVoeu(1, null));
		} catch (AffectationVoeuImpossibleException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	void testGroupeAjouterVoeuSujetDejaChoisi() {
		Sujet s = new Sujet("suj1");
		try {
			assertTrue(g.addVoeu(1, s));
		} catch (AffectationVoeuImpossibleException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		AffectationVoeuImpossibleException thrown = assertThrows(AffectationVoeuImpossibleException.class, () -> {g.addVoeu(2, s);});
        assertEquals("Le sujet a déjà été choisi", thrown.getMessage());
	}
	
	
	
	@Test
	void testGroupeSetVoeu() {
		HashMap<Integer, Sujet> v = new HashMap<Integer, Sujet>();
		Sujet s = new Sujet("suj1");
		v.put(1, s);
		g.setVoeux(v);
		AffectationVoeuImpossibleException thrown = assertThrows(AffectationVoeuImpossibleException.class, () -> {g.addVoeu(2, s);});
        assertEquals("Le sujet a déjà été choisi", thrown.getMessage());
	}
	
	@Test
	void testGroupeConstructorAllInfo() {
		Groupe gg = new Groupe(444, 444, "gg", new HashMap<Integer, Sujet>(), false);
		assertFalse(gg.isVoeuReady());
		assertEquals("gg", gg.getNom());
		
	}
	
	@Test
	void testGroupeAddSixVoeuxAndReady() {
		Sujet s1 = new Sujet("suj");
		Sujet s2 = new Sujet("suj");
		Sujet s3 = new Sujet("suj");
		Sujet s4 = new Sujet("suj");
		Sujet s5 = new Sujet("suj");
		Sujet s6 = new Sujet("suj");
		try {
			g.addVoeu(1, s1);
			g.addVoeu(2, s2);
			g.addVoeu(3, s3);
			g.addVoeu(4, s4);
			g.addVoeu(5, s5);
			g.addVoeu(6, s6);
			assertTrue(g.getVoeuFini());
		} catch (AffectationVoeuImpossibleException e) {
			assertFalse(true);
			e.printStackTrace();
		}
	}
	
	@Test
	void testGroupeAddOneVoeuAndNotReady() {
		Sujet s = new Sujet("suj");
		try {
			g.addVoeu(1, s);
		} catch (AffectationVoeuImpossibleException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		assertFalse(g.getVoeuFini());
	}
}
