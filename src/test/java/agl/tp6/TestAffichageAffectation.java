package agl.tp6;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

import static agl.tp6.AffichageAffectation.affectationsVersHtml;
import static agl.tp6.AffichageAffectation.generationAleatoireGestionTer;
import static org.junit.jupiter.api.Assertions.*;

public class TestAffichageAffectation {

    @Test
    public void testAffectationsVersHtml(){
        GestionTER gestionTER = new GestionTER();

        /*création des groupes et des sujets, et affectation de ces derniers*/
        Groupe groupe1 = new Groupe("groupe1");
        Sujet sujet1 = new Sujet("sujet1");
        gestionTER.addGroupe(groupe1);
        gestionTER.addSujet(sujet1);
        groupe1.setAffectation(sujet1);

        Groupe groupe2 = new Groupe("groupe2");
        Sujet sujet2 = new Sujet("sujet2");
        gestionTER.addGroupe(groupe2);
        gestionTER.addSujet(sujet2);
        groupe2.setAffectation(sujet2);
        affectationsVersHtml(gestionTER, "affectation_test.html");
        File fichierAffichageHtml = new File("affectation_test.html");

        assertAll(
                ()->assertTrue(fichierAffichageHtml.exists()),
                ()->assertEquals("<style type=\"text/css\">\n" +
                        ".tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}\n" +
                        ".tftable th {font-size:12px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}\n" +
                        ".tftable tr {background-color:#d4e3e5;}\n" +
                        ".tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}\n" +
                        ".tftable tr:hover {background-color:#ffffff;}\n" +
                        "</style><table class=\"tftable\" border=\"1\">\n" +
                        "<tr><th>Nom des groupes:</th><th>groupe1</th><th>groupe2</th></tr>\n" +
                        "<tr><th>Nom du sujet affecté:</th><td>sujet1 (id:" + sujet1.getId() + ")</td><td>sujet2 (id:" + sujet2.getId() + ")</td></tr>\n" +
                        "</table>", Files.readString(Path.of("affectation_test.html")))
        );
        fichierAffichageHtml.delete();
    }

    @Test
    public void testGenerationAleatoireGestionTer(){
        GestionTER gestionTer = generationAleatoireGestionTer(3);
        assertAll(
                ()->assertEquals(3, gestionTer.getNbGroupe()),
                ()->assertEquals(3, gestionTer.getSujets().size()),
                ()->assertNotEquals(null, gestionTer.getGroupe(0).getAffectation())
        );
    }

}
