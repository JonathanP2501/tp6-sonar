package agl.tp6;

import java.util.HashMap;

import agl.tp6.exception.AffectationVoeuImpossibleException;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Groupe {
  //attributs
  private int id;
  private static int idPartage;
  private String nom;
  private HashMap<Integer, Sujet> voeux;
  private static final int maxVoeu = 6;
  private boolean voeuFini;
  private Sujet affectation;
  private String prof;
  
  //constructeurs
  public Groupe() {}
  public Groupe(int id, int idPartage, String nom, HashMap<Integer, Sujet> voeux, boolean voeuFini) {
    this.id = id;
    Groupe.idPartage = idPartage;
    this.nom = nom;
    this.voeux = voeux;
    this.voeuFini = voeuFini;
  }  
  public Groupe(String nom) {
    this.id = idPartage;
    idPartage = idPartage + 1;
    this.nom = nom;
    this.voeux = new HashMap<>();
    this.voeuFini = false;
  }
  
  //getter/setter
  public String getProf() {
	  return prof;
  }
  public void setProf(String prof) {
	  this.prof = prof;
  }
  public Sujet getAffectation() {
	  return this.affectation;
  }
  public void setAffectation(Sujet s) {
	  this.affectation = s;
  }
  public boolean getVoeuFini() {
	  return this.voeuFini;
  }
  public void setVoeuFini(boolean voeuFini) {
    this.voeuFini = voeuFini;
  }
  public void setNom(String nom) {
    this.nom = nom;
  }
  public String getNom() {
    return this.nom;
  }
  public int getId() {
    return this.id;
  }
  public void setVoeux(HashMap<Integer, Sujet> hash) {
    this.voeux = hash;
  }  
  public boolean isVoeuReady() {
    return this.voeuFini;
  }
  
  //méthodes
  /**
   * Verifie si un sujet a déjà été ajouté aux voeux
   * @param Sujet suj
   * @return boolean true s'il est présent dans la liste
   */
  private boolean isSujetInVoeu(Sujet suj) {
    boolean inIt = false;
    for(Integer key : this.voeux.keySet()) {
      if(this.voeux.get(key) == suj) {
        inIt = true;
      }
    }
    return inIt;
  }
  
  /**
   * donne l'ordre d'un sujet s'il a été ajouté aux voeux
   * @param Sujet suj
   * @return ordre int, -1 s'il n'est pas dans les voeux
   */
  public int getOrdreSujet(Sujet s) {
	int ordre = -1;
    for(Integer key : this.voeux.keySet()) {
      if(this.voeux.get(key) == s) {
        ordre = key;
      }
    }
    return ordre;
  }
  /**
   * Ajoute un voeu à la liste
   * @param Sujet suj
   * @param int ordre (entre 1 et 6, ordre de priorité) 
   * @return boolean true si le voeu a été correctement ajouté
   */
  public boolean addVoeu(int ordre, Sujet suj) throws AffectationVoeuImpossibleException {
    if(ordre > 0 && ordre <= maxVoeu) {
      if(!this.isSujetInVoeu(suj)) {
        if(!this.voeux.keySet().contains(ordre)) {
          this.voeux.put(ordre, suj);
        } else {	
        	throw new AffectationVoeuImpossibleException("L'ordre a déjà été rempli");
        }
      } else {
    	  throw new AffectationVoeuImpossibleException("Le sujet a déjà été choisi");
      }
    } else {
    	throw new AffectationVoeuImpossibleException("L'ordre du voeu doit être entre 1 et 6");
    }
    boolean complet = true;
    for(int i=1; i<=maxVoeu; i++) {
      if(this.voeux.get(i) == null) {
        complet = false;
      }
    }
    if(complet) {
      this.voeuFini = true;
    }
    return true;
  }
  /**
   * toString
   * @return String instance bien formée
   */
  public String toString() {
    String str = this.nom + " (id:" + this.id + ")\n"; 
    for(Integer key : this.voeux.keySet()) {
      str += "--" + key + ": " + this.voeux.get(key).toString() + "\n";
    }
    return str;
  }

}