package agl.tp6;

public class Sujet {
  //attributs
  private static int index;
  private String titre;
  private int id;
  
  //constructeurs
  public Sujet() {}
  public Sujet(String titre) {
    this.setTitre(titre);
    this.setId(index);
    index++;
  }
  
  //getter/setter
  public String getTitre() {
    return this.titre;
  }
  public void setTitre(String titre) {
    this.titre = titre;
  }
  public int getId() {
    return this.id;
  }
  public void setId(int id) {
	  this.id = id;
  }
  
  //méthodes
  /**
   * toString
   * @return String instance bien formée
   */
  public String toString() {
    return this.titre + " (id:" + this.id + ")";
  }
}